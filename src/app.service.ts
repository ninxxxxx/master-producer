import { Injectable } from '@nestjs/common';
import { KafkaClient, Producer } from 'kafka-node'

export const KAFKA_HOST = 'kafka0.nize.ai:29092'

@Injectable()
export class AppService {

  private client = new KafkaClient({ kafkaHost: KAFKA_HOST })
  private producer = new Producer(this.client)
  constructor() {
    this.producer.on('ready', () => {
      console.log('Producer is Ready!');
    })
  }

  root(): string {
    return 'Hello World!';
  }

  public shoot(payloads: any[]): Promise<any> {
    return new Promise((resolve, reject) => {
      this.producer.send(payloads, (err, data) => {
        if (err)
          reject(err);
        else
          resolve(data)
      })

    })
  }



}
