import { Get, Controller, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { } from 'kafka-node'
import { ProduceRequest } from 'kafka-node';
import { Response } from 'express';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  root(): string {
    return this.appService.root();
  }


  @Get('producesbl')
  async produceSBL(@Res() res: Response) {
    const payloads: ProduceRequest[] = [{
      topic: 'nizeapi.switchbotlog',
      messages: [JSON.stringify({
        clientId: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        systemId: 'dev',
        channel:
        {
          channelId: '458318158024707',
          channelName: 'FB Nize Kati Dev2'
        },
        botStatus: 'on',
        createdDate: (new Date()).toISOString(),
        createdBy: '',
        details:
        {
          status: '[on] - manual',
          description: 'กำลังใช้งานอยู่',
          requiredTime: false
        }
      })]
    }]
    try {
      const result = await this.appService.shoot(payloads);
      console.log('result', result);
      res.json(result);

    } catch (e) {
      console.log(e);
    }
  }

  @Get('producesbt')
  async produceSWT(@Res() res: Response) {
    const payloads: ProduceRequest[] = [{
      topic: 'nizeapi.switchbottype',
      messages: [JSON.stringify({
        clientId: 'aaaaaaaaaaaaaaaaaaaaaaaa',
        systemId: 'dev',
        channel:
        {
          channelId: '458318158024707',
          channelName: 'FB Nize Kati Dev2'
        },
        botType: { _id: '5bf50542b94c8a07', botTypeName: 'ทดสอบ' },
        createdDate: (new Date()).toISOString(),
        createdBy: '07abac191182db4f0b0b06974ecc7e6056fe2f77a37e2d76810bb25a3bc3785bb5'
      })]
    }]

    try {
      const result = await this.appService.shoot(payloads);
      console.log('result', result);
      res.json(result);

    } catch (e) {
      console.log(e);
    }
  }

}